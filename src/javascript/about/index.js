const aboutNode = document.getElementById("about-video");

const videoNode = document.getElementById("about-video-container")

const playAboutVid = () => {
    aboutNode.style.display = "block";
    videoNode.requestFullscreen();
    videoNode.play();


}

const closeAboutVid = () => {
    videoNode.pause();
    videoNode.currentTime = 0;
    aboutNode.style.display = "none";
}