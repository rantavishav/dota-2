import { getAllHeroData } from "./api/heros.js";
import { endLoader } from "./loader/index.js";

const createSingleImage = (element, item, imgSrc, imgClass ) => {
    // console.log(`item`, item)
    // Adding image-container element
    const node = document.createElement("div");
    node.classList.add("attribute-img-container");
    node.style.backgroundImage = `url(https://api.opendota.com${item.img})`;
    node.setAttribute("id", item.id);
    node.addEventListener('click', handleClicked);
    element.appendChild(node);
    // Adding attribute image-container  
    const imgNode = document.createElement("img");
    imgNode.classList.add(imgClass);
    imgNode.setAttribute("src",imgSrc);
    node.appendChild(imgNode);
    //Adding hero name container 
    const nameNode = document.createElement('h2');
    nameNode.innerText = item.localized_name;
    node.appendChild(nameNode);

}

const createImageGrid = (data) =>{
    const htmlCollection = document.getElementsByClassName("attr-grid");
    const docArr = [].slice.call(htmlCollection); //coverting htmlCollection Type to Array
    
    const str = data.filter((item)=> item.primary_attr === "str");//All str heroes array
    const agi = data.filter(item=> item.primary_attr === "agi");//All agi heroes array
    const int = data.filter(item=> item.primary_attr === "int");//All int heroes array
   
    docArr.forEach(element => {
        
        if (element.id === "str-container") {
            str.forEach(item => {

                createSingleImage(element, item ,`../assest/img/attributes/str.png`, 'str-img')
        });
        } else if(element.id === "agi-container") {
            agi.forEach(item => {

                createSingleImage(element, item ,`../assest/img/attributes/agi.png`, 'agi-img')

        });
        }else if(element.id === "int-container"){
            int.forEach(item => {

                createSingleImage(element, item ,`../assest/img/attributes/int.png`, 'int-img')         

        });
        }
     });
     
}

const handleClicked = (event) =>{
    console.log("Id is", event);
    window.location.href = `../html/heros/hero.html?id=${event.target.id}`
}


const main = async () =>{

   const data = await getAllHeroData();
   
   createImageGrid(data)
   
   endLoader(); //remove Loader from DOM
}


window.onload=()=>{
    main();
}


