import { getAllHeroData } from '../api/heros.js';
import { endLoader } from '../loader/index.js';

const main =async ()=>{
    const data = await getAllHeroData();
    // Seacrh for ID from url query params
    // console.log(`data `, data )
    const urlSearchParams = new URLSearchParams(window.location.search);
    const id = Object.fromEntries(urlSearchParams.entries()).id;
    
    const heroData = data.filter(item=> item.id === Number(id))[0];
    console.log(`heroData`, heroData.localized_name)
    renderHeader(heroData.localized_name);
    renderHeroDetails(heroData);
    endLoader();
    
}

const renderHeader = (headerData) => {
    document.getElementById("hero_header").innerHTML = headerData;
}

const renderHeroDetails = (heroData) => {

    // Render Title
    document.title = heroData.localized_name;
    const node = document.createElement("link");
    node.setAttribute("rel","shortcut icon");
    node.setAttribute("href",`https://api.opendota.com${heroData.icon}`);
    document.head.appendChild(node)
    // Render Hero Image
    document.getElementById("hero-image").src = `https://api.opendota.com${heroData.img}`;
    // document.getElementById("hello").src = `../../assest/heroes/${heroData.name}.webm`;
    // document.getElementById("hello").defaultPlaybackRate = 0.1;
    // document.getElementById("hello").playbackRate = 0.6;

    // Render Max Health And Max Mana
    const  [maxHealth, totalRegen, maxMana, totalManaRegen]  = totalHealth_Mana(heroData);
    document.getElementById("total-health").innerHTML = maxHealth;
    document.getElementById("health-regen").innerHTML = totalRegen;
    document.getElementById("total-mana").innerHTML = maxMana;
    document.getElementById("mana-regen").innerHTML = totalManaRegen;

    // Render Roles
    heroData.roles.forEach(element => {

        document.getElementById(`${element}-rectangle`).classList.remove("rectangle");
        document.getElementById(`${element}-rectangle`).classList.add("rectangle-active");
        
    });

    // Render Attributes Data
    document.getElementById("base-str").innerHTML = `${heroData.base_str} `;
    document.getElementById("base-agi").innerHTML = `${heroData.base_agi} `;
    document.getElementById("base-int").innerHTML = `${heroData.base_int} `;
    document.getElementById("str-gain-per-level").innerHTML = ` + ${heroData.str_gain} per level`;
    document.getElementById("agi-gain-per-level").innerHTML = ` + ${heroData.agi_gain} per level`;
    document.getElementById("int-gain-per-level").innerHTML = ` + ${heroData.int_gain} per level`;

    // Render Attack Data
    document.getElementById("attack-damage").innerHTML = `${heroData.base_attack_min} - ${heroData.base_attack_max}`;
    document.getElementById("b_a_t-damage").innerHTML = `${heroData.attack_rate}`;
    document.getElementById("attack_range-damage").innerHTML = `${heroData.attack_range}`;
    document.getElementById("projectile_speed-damage").innerHTML = `${heroData.projectile_speed}`;

    // Render Defense Data
    document.getElementById("armor-defense").innerHTML = `${heroData.base_armor} + ${(heroData.base_agi * 0.17).toFixed(2)}`;
    document.getElementById("magic-resist-defense").innerHTML = `${heroData.base_mr}%`;

    // Render Mobility Data
    document.getElementById("movement-mobility").innerHTML = heroData.move_speed;
    document.getElementById("turn-rate-mobility").innerHTML = Number(heroData.turn_rate);
    


}

const totalHealth_Mana = (heroData) => {
    // calculte max health and regen
    const baseHealth = heroData.base_health;
    const baseStr = heroData.base_str

    const maxHealth = baseHealth + (baseStr*20);
    const totalRegen = (heroData.base_health_regen + (baseStr*0.1)).toFixed(2);

    // calaculate max mana and manaRegen
    const maxMana = heroData.base_mana + (heroData.base_int*12);
    const totalManaRegen =( heroData.base_mana_regen + (heroData.base_int*0.05)).toFixed(2);

    return [maxHealth, totalRegen, maxMana, totalManaRegen];
}
window.onload=()=>{
    main()
}

